/*******************************************************************************
 * Copyright (c) 2017-2018, Kyle Robbertze (AIMS, South Africa)
 *
 * This project is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License version 3 as 
 * published by the Free Software Foundation.
 *
 * It is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this project. If not, see <http://www.gnu.org/licenses/>.
 *
 *    Description:  Main entry point for Hashcheck
 * 
 *        Created:  23/02/2017 10:02:51
 *       Compiler:  g++
 * 
 *         Author:  Kyle Robbertze (kr), kyle@aims.ac.za
 *******************************************************************************/
#include <algorithm>
#include <iostream>
#include <stdlib.h>
#include <string>

#include <QApplication>
#include <QWidget>

#include "main_window.h"

bool cmdOptionExists(char* begin[], char* end[], const std::string& option) {
    return std::find(begin, end, option) != end;
}

int main(int argc, char* argv[]) {
    if (cmdOptionExists(argv, argv+argc, "-v") ||
            cmdOptionExists(argv, argv+argc, "--version")) {
        std::cout << "Hashcheck " << HASHCHECK_VERSION << "\n";
        std::cout << "This project comes with NO WARRANTY, to the extent permitted by the law.\n";
        std::cout << "You may redistribute it under the terms of the GNU General Public License\n";
        std::cout << "version 3; see the file named LICENSE for details.\n\n";
        std::cout << "Written by Kyle Robbertze\n";
        return EXIT_SUCCESS;
    }

    if (argc == 1) {
        QApplication app { argc, argv };
        MainWindow window{};
        window.setWindowTitle("Hashcheck");
        window.show();

        return app.exec();
    }
    std::cout << "usage: hashcheck [-h] [-v]\n\n";
    std::cout << "optional arguments:\n";
    std::cout << "-h, --help\tshow this help message and exit\n";
    std::cout << "-v, --version\toutput the version information and exit\n";
    return EXIT_SUCCESS;
}
