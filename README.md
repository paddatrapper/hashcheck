# Hashcheck

Hashcheck provides a graphical interface to verifying all the files on an ISO
that makes up a live environment.

## Dependencies

Hashcheck depends on QT5, pv and md5sum (installed by default on most sane systems)
and uses cmake to build. On Debian based systems these can be installed with

    $ sudo apt install cmake pv qt5-default

## Building

    $ mkdir build && cd build
    $ cmake ..
    $ make

## Running

    $ hashcheck [-h] [-v]

`-h` displays the help message and `-v` displays the version information.

It assumes that the live environment is mounted under `/lib/live/mount/medium/`.
The md5 sum is expected to be in the file `/lib/live/mount/medium/md5sum.txt`,
in the format supplied by `md5sum`
