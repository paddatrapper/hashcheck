/*******************************************************************************
 * Copyright (c) 2017-2018, Kyle Robbertze (AIMS, South Africa)
 *
 * This project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * It is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this project. If not, see <http://www.gnu.org/licenses/>.
 *
 *    Description:  A MD5 sum check process
 *
 *        Created:  20/01/2018 10:28:04
 *       Compiler:  g++
 *
 *         Author:  Kyle Robbertze (kr), kyle@aims.ac.za
 *******************************************************************************/

#ifndef MD5_PROCESS_H
#define MD5_PROCESS_H

#include <QFile>
#include <QFileInfo>
#include <QProcess>
#include <QObject>

class MD5Process : public QObject {
    Q_OBJECT
    public:
        MD5Process();
        MD5Process(QFile& file, QString expectedMD5);
        MD5Process(const MD5Process& other);
        void start();
        QString fileName() const;
        QString actualMD5() const;
        QString expectedMD5() const;

        enum Result {
            IDENTICAL,
            DIFFERENT,
            ERROR
        };
        MD5Process& operator=(MD5Process arg);
    signals:
        void progressChanged(int progress);
        void finished(Result result, int exitCode, MD5Process* process);
    private slots:
        void onOutputReady();
        void onFinish(int exitCode, QProcess::ExitStatus exitStatus);
    private:
        QString m_actualMD5;
        QString m_expectedMD5;
        QFile m_file;
        QFileInfo m_fileInfo;
        QProcess m_process;
};

bool operator==(const MD5Process& lhs, const MD5Process& rhs);
inline bool operator!=(const MD5Process& lhs, const MD5Process& rhs) { return !(lhs == rhs); };

#endif // MD5_PROCESS_H
